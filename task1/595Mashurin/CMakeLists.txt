add_definitions(-D GLM_ENABLE_EXPERIMENTAL)

include_directories(common)

set(SRC_FILES
        common/Application.cpp
        common/DebugOutput.cpp
        common/Camera.cpp
        common/Mesh.cpp
        common/ShaderProgram.cpp
        MoebiusStrip.cpp
        )

set(HEADER_FILES
        common/Application.hpp
        common/DebugOutput.h
        common/Camera.hpp
        common/Mesh.hpp
        common/ShaderProgram.hpp
        )

set(SHADER_FILES
        595MashurinData1/shader.frag
        595MashurinData1/shaderNormal.vert
        )

MAKE_OPENGL_TASK(595Mashurin 1 "${SRC_FILES}")