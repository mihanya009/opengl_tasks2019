Репозиторий для заданий по компьютерной графике в МФТИ
======================================================

```bash
cd task1/592Litvintseva/
mkdir build
cd build
cmake ..
make -j 8
./bin
./task
```
