#include "common/Application.hpp"
#include "common/Mesh.hpp"
#include "common/Camera.hpp"
#include "common/ShaderProgram.hpp"
#include "PerlinNoise.hpp"

#include <vector>

using PointData = std::vector<std::pair<int, glm::vec3>>;
using DataNormals = std::vector<std::vector<PointData>>;

MeshPtr BuildTerrain(size_t freq, float size) {
	std::vector<glm::vec3> verts;
	DataNormals point2normals(freq + 1, std::vector<PointData>(freq + 1, PointData()));

	PerlinNoise pn;
	pn.LoadBuffer("buffer.txt");

	float step = 2.0f / freq;
	size_t index_x = 0;
	size_t index_y = 0;
	size_t num = 0;
	for (float i = -1.0f, index_x = 0; i < 1.0f; i+=step, ++index_x) {
		for (float j = -1.0f, index_y = 0; j < 1.0f; j+=step, ++index_y) {
			glm::vec3 v1(i * size, j * size, pn.Noise(i, j));
			glm::vec3 v2((i + step) * size, j * size, pn.Noise(i + step, j));
			glm::vec3 v3(i * size, (j + step) * size, pn.Noise(i, j + step));
			glm::vec3 v4((i + step) * size, (j + step) * size, pn.Noise(i + step, j + step));

			verts.push_back(v1);
			verts.push_back(v2);
			verts.push_back(v3);
			verts.push_back(v2);
			verts.push_back(v3);
			verts.push_back(v4);

			glm::vec3 norm1 = glm::normalize(glm::cross(v3 - v1, v2 - v1));
			glm::vec3 norm2 = glm::normalize(glm::cross(v3 - v2, v4 - v2));

			point2normals[index_x][index_y].push_back(std::make_pair(num++, norm1));
			point2normals[index_x + 1][index_y].push_back(std::make_pair(num++, norm1));
			point2normals[index_x][index_y + 1].push_back(std::make_pair(num++, norm1));
			point2normals[index_x + 1][index_y].push_back(std::make_pair(num++, norm2));
			point2normals[index_x][index_y + 1].push_back(std::make_pair(num++, norm2));
			point2normals[index_x + 1][index_y + 1].push_back(std::make_pair(num++, norm2));

		}
	}

	std::vector<glm::vec3> normals(num);
	for (int i = 0; i < point2normals.size(); i++) {
		for (int j = 0; j < point2normals.size(); j++) {
			glm::vec3 res_norm;
			for (const auto& norm : point2normals[i][j]) {
				res_norm += norm.second;
			}
			res_norm /= point2normals[i][j].size();
			for (const auto& norm : point2normals[i][j]) {
				normals[norm.first] = res_norm;
			}
		}
	}

	DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf0->setData(verts.size() * sizeof(float) * 3, verts.data());

	DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

	MeshPtr mesh = std::make_shared<Mesh>();
	mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
	mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
  	mesh->setPrimitiveType(GL_TRIANGLES);
	mesh->setVertexCount((GLuint)verts.size());
	return mesh;
}


class TerrainApplication : public Application {
public:
	void makeScene() override {
		Application::makeScene();

		_cameraMover = std::make_shared<FreeCameraMover>();

		_mesh = BuildTerrain(500U, 3.0f);
		_mesh->setModelMatrix(glm::mat4(1.0f));

		_shader = std::make_shared<ShaderProgram>("591KozlovData1/shader.vert", "591KozlovData1/shader.frag");
	}

	void draw() override {
		Application::draw();

		int width, height;

		glfwGetFramebufferSize(_window, &width, &height);

		glViewport(0, 0, width, height);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		_shader->use();
		_shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		_shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
		_shader->setMat4Uniform("modelMatrix", _mesh->modelMatrix());

		_mesh->draw();
	}

private:
	MeshPtr _mesh;
	ShaderProgramPtr _shader;
};


int main() {
	TerrainApplication app;
	app.start();
	
	return 0;
}
