set(SRC_FILES
Application.cpp
Application.hpp
Camera.cpp
Camera.hpp
Common.h
DebugOutput.cpp
DebugOutput.h
Main.cpp
Mesh.cpp
Mesh.hpp
ShaderProgram.cpp
ShaderProgram.hpp
Texture.cpp
Texture.hpp
)

MAKE_OPENGL_TASK(596Iglina 1 "${SRC_FILES}")
