/*
Преобразует координаты вершины из локальной системы координат в Clip Space.
Копирует цвет вершины из вершинного атрибута в выходную переменную color.
*/

#version 330

#extension GL_ARB_shading_language_420pack : require

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec4 vertexColor;

out vec4 color;

void main()
{
//    color = vertexColor;
    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);

    const int HeightsNumber = 6;

    vec3 Colors[HeightsNumber] = {
        vec3(0, 0, 255), // blue -- 0
        vec3(0, 255, 96), // green - 200m
        vec3(255, 255, 0), // yellow -- 697
        vec3(255, 19, 0), // red -- 1367m
        vec3(255, 0, 157), // pink -- 2000m
        vec3(255, 255, 255) , // white -- >= 4000m
    };

    float Heights[HeightsNumber] = {
        0.f,
        200.f,
        697.f,
        1367.f,
        2000.f,
        4000.f,
    };

    for (int i = 0; i < HeightsNumber; ++i) {
        Colors[i] /= 255;
        Heights[i] /= Heights[HeightsNumber - 1];
    }

    float z = vertexPosition[2];

    vec3 color3 = vec3(1, 1, 1);
    float alpha;
    for (int i = 1; i < HeightsNumber; ++i) {
        if (z < Heights[i]) {
            alpha = (Heights[i] - z) / (Heights[i] - Heights[i - 1]);
            color3 = Colors[i - 1] * alpha + Colors[i] * (1 - alpha);
            break;
        }
    }
    color = vec4(color3, 1.f);
}
